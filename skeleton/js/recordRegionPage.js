// Code for the Record Region page.
var map, currentPosition, currentAccuracy, currentPositionCircle, regionPolygon;

//Initialises a new region constructor from shared.js
var region = new Region
var regionMarkers = []

//Initializes the google maps
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 17
  });

  //Loads saved regions from local storage if there are any
  //Starts tracking position
  loadSavedRegions();
  startTrackingPosition();
}

//Function to watch and determine current position
function startTrackingPosition() {
  // Try HTML5 geolocation.
  if (navigator.geolocation) {
    // Watches for position change
    navigator.geolocation.watchPosition(function(position) {
      console.log(position);
      var newPosition = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      }
      
      // Gets the current accuracy
      var newAccuracy = position.coords.accuracy

      // Handles changes to position and accuracy
      positionChanged(newPosition, newAccuracy);
    }, function() {
      // Geolocation service failed or not allowed
      handleLocationError(true);
    });
  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false);
  }
}

// Replaces current position
function positionChanged(newPosition, newAccuracy){
  currentPosition = newPosition
  currentAccuracy = newAccuracy
  
  drawCurrentPositionCircle()
  displayCurrentAccuracy()

  map.setCenter(currentPosition);
}

function drawCurrentPositionCircle() {
  // Clear outdated circle if it exists
  if(currentPositionCircle !== undefined){
    currentPositionCircle.setMap(null);
  }

  // Draw new position circle
  currentPositionCircle = new google.maps.Circle({
    strokeColor: '#FF0000',
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: '#FF0000',
    fillOpacity: 0.35,
    map: map,
    center: currentPosition,
    radius: currentAccuracy
  });
}

function displayCurrentAccuracy() {
  // Updates the DOM to show current accuracy
  document.getElementById('currentAccuracy').innerHTML =
    'Current Accuracy: ' + currentAccuracy + ' metres'
}

// Handles location error
function handleLocationError(browserHasGeolocation) {
  displayMessage(
    browserHasGeolocation ?
    'Error: The Geolocation service failed.' :
    'Error: Your browser doesn\'t support geolocation.'
  );
}

// Adds corner based on current position
function addCorner() {
  // Unable to add corner if current accuracy is too low/more than 10
  if(currentAccuracy > 10){
    displayMessage('Current position is only accurate to ' + currentAccuracy + ' metres and is too low!')
  }else{
    region.addCorner(currentPosition);
    drawRegionPolygon();
    drawCornerMarker();
  }
}
// Deletes old polygons
function drawRegionPolygon() {
  if(regionPolygon !== undefined){
    regionPolygon.setMap(null);
  }
  // Draws a polygon with the corners as path
  regionPolygon = new google.maps.Polygon({
    paths: region.corners,
    strokeColor: '#FF0000',
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: '#FF0000',
    fillOpacity: 0.35,
    map: map
  });
}
// Adds a marker at the same location when each corner is added
function drawCornerMarker(){
  var marker = new google.maps.Marker({
    position: currentPosition,
    map: map,
  });

  // Tracks drawn markers for easy removal
  regionMarkers.push(marker);
}

function resetRegion(){
  // Reset region to new instance
  region = new Region

  // Remove Region Polygon on map
  regionPolygon.setMap(null);

  // Delete all markers on map
  regionMarkers.forEach(function(marker){
    marker.setMap(null)
  });
}

function saveRegion(){
  // Getting region name from DOM
  var name = document.getElementById('regionName').value

  // Only save regions with name and has at least 3 corners
  if(region.corners.length < 3){
    displayMessage('Cannot Save: Regions must have at least 3 corners!')
    // Must input a name to save the region
  }else if(name.length == 0){
    displayMessage('Cannot Save: Region name cannot be empty!')
  }else{
    // Update the created at timestamp for region object
    region.setName(name)
    region.updateCreatedAt()

    // Calling storeRegion function from shared.js to interact with localstorage
    storeRegion(region)

    // Calls the function to send user to index page
    redirectToIndex()
  }
}
// Redirects the user to index page
function redirectToIndex() {
  window.location.href = 'index.html'
}
