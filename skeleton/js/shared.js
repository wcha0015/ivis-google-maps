// Shared code needed by the code of all three pages.

// Constructor for Region object
function Region(attributes = {name: null, corners: [], createdAt: null,  boundaryFencePosts:[]}){
  // Properties
  // Name of region, Position of corners, Time at where the corners were created, Array of boundary fence posts locations
  this.name = attributes.name
  this.corners = attributes.corners.map(function(corner){
                    return new google.maps.LatLng(corner)
                  });
  this.createdAt = attributes.createdAt
  this.boundaryFencePosts = attributes.boundaryFencePosts

  // Public methods
  // Pushes lat lng instances of current position from record region into corners array
  this.addCorner = function(latlng){
    this.corners.push(new google.maps.LatLng(latlng));
  }
  // Lists down the time of when each region is created
  this.updateCreatedAt = function(){
    this.createdAt = Date();
  }
  // Sets a name for each region
  this.setName = function(name){
    this.name = name
  }
  // Calculates the area of the region
  this.calculateArea = function(){
    // uses google maps geometry library to calculate area
    return google.maps.geometry.spherical.computeArea(this.corners);
  }
  // Calculates the perimeter of the region
  this.calculatePerimeter = function(){
    // Duplicate the first corner of the region to the end of the array
    // so that we close the loop of the region, allowing google maps to
    // calculate the perimeter for us
    var loopCorners = this.corners
    loopCorners.push(loopCorners[0])

    return google.maps.geometry.spherical.computeLength(loopCorners);
  }
  
  // Calculates number of posts needed to fence up the corresponding region perimeter
  this.calculatePosts = function(){
    //Picks the location for first and last post
    var firstPost = this.corners[0];
    var lastPost = this.corners.length-1 ;
    
  // Uses for loop to determine how big the span is between each post ( incomplete : just an idea)
    for(var k=0; k<regionArea.length;k++)
    {
        if (var span < 4)
      {
        var span = regionArea%2 == 0;
      }
    }
    var posts = return google.maps.geometry.spherical.interpolate(firstPost,lastPost,span*k)
  // Pushes the posts into the fence posts array
    this.boundaryFencePosts.push(posts)
  }
}

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.eng1003.fencingApp";

// Array of saved Region objects.
var savedRegions = [];

//Function to save the current region into local storage
function storeRegion(region){
  // Push region into existing regions array
  savedRegions.push(region);

  saveRegionsToLocalStorage();
}

// Place the new regions array into local storage
function saveRegionsToLocalStorage(){
  localStorage.setItem(APP_PREFIX, JSON.stringify(savedRegions));
}
// Loads the region and its information from local storage
function loadSavedRegions(){
  var savedRecords = JSON.parse(localStorage.getItem(APP_PREFIX));

// Returns a new region 
  if(savedRecords !== null){
    savedRegions = savedRecords.map(function(record){
      return new Region(record)
    })
  }
}

// This function displays the given message String as a "toast" message at
// the bottom of the screen.  It will be displayed for 2 second, or if the
// number of milliseconds given by the timeout argument if specified.
function displayMessage(message, timeout)
{

  if (timeout === undefined)
  {
    // Timeout argument not specifed, use default.
    timeout = 2000;
  }

  if (typeof(message) == 'number')
  {
    // If argument is a number, convert to a string.
    message = message.toString();
  }

  if (typeof(message) != 'string')
  {
    console.log("displayMessage: Argument is not a string.");
    return;
  }

  if (message.length == 0)
  {
    console.log("displayMessage: Given an empty string.");
    return;
  }

  var snackbarContainer = document.getElementById('toast');
  var data = {
    message: message,
    timeout: timeout
  };

  if (snackbarContainer && snackbarContainer.hasOwnProperty("MaterialSnackbar"))
  {
    snackbarContainer.MaterialSnackbar.showSnackbar(data);
  }
}
