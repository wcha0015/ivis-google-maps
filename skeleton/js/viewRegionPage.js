// Code for the View Region page.
var map, regionIndex, selectedRegion

//Re-initializes the map
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 17
  });

  // Load saved regions via shared.js 
  loadSavedRegions();
  loadSelectedRegion();
}

// Load the region that was selected in the main page
function loadSelectedRegion(){
  regionIndex = localStorage.getItem(APP_PREFIX + ".selectedRegion");

  // If no region was selected yet, or selected region doesn't exist,
  // send the user to index page
  if(regionIndex === null || regionIndex > savedRegions.length){
    redirectToIndex();
  }else{
    selectedRegion = savedRegions[regionIndex]
    //Calls some functions to update the map with region information
    updateHeader();
    displayRegionPolygon();
    displayRegionMarkers();
    fitMapToRegion();
    showRegionArea();
    showRegionPerimeter();
  }
}

// Displays the polygon based on the corners of the selected region
function displayRegionPolygon() {
  new google.maps.Polygon({
    paths: selectedRegion.corners,
    strokeColor: '#FF0000',
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: '#FF0000',
    fillOpacity: 0.35,
    map: map
  });
}
// Display the markers based on the markers of the selected region
function displayRegionMarkers(){
  selectedRegion.corners.forEach(function(corner){
    new google.maps.Marker({
      position: corner,
      map: map
    });
  })
}
// Fit all the markers in the map by zooming out
function fitMapToRegion(){
  var bounds = new google.maps.LatLngBounds();
  selectedRegion.corners.forEach(function(corner){
    bounds.extend(corner)
  })
  map.fitBounds(bounds);
}
// Updates the header with the name of the selected region
function updateHeader(){
  document.getElementById("headerBarTitle").textContent = `Viewing Region: ${selectedRegion.name}`
}
// Remove the region selected from local storage and redirect the user to index page
function removeRegion(){
  savedRegions.splice(regionIndex, 1)

  saveRegionsToLocalStorage();

  redirectToIndex();
}
// Function to direct the user to index page
function redirectToIndex(){
  window.location.href = 'index.html'
}
// Displays the value of the region area 
function showRegionArea(){
  document.getElementById("regionArea").textContent = `Area: ${selectedRegion.calculateArea()} squared metres`
}
// Displays the value of the region perimeter
function showRegionPerimeter(){
  document.getElementById("regionPerimeter").textContent = `Perimeter: ${selectedRegion.calculatePerimeter()} metres`
}
// Places the posts using interpolation
function placePosts(){

}
// Displays all the posts required to fence up the perimeter
function togglePost(){
  placePosts()
  
}