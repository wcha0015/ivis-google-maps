// Code for the main app page (Regions List).
function loadRegionsList(){
  // Call function from shared.js to load regions from localstorage
  // this populates a 'savedRegions' array found in shared.js with instances of Regions loaded from localstorage
  loadSavedRegions();

  // Using the 'savedRegions' array, we iterate through the Region objects
  // and append them to the list in the DOM
  listRegions();
}

function listRegions(){
  // Find the <ul> node in the DOM
  var ulNode = document.getElementById("regionsList")

  // Loop through each Region in the savedRegions array, with index for easy navigation
  savedRegions.forEach(function(region, index){
    // Create the <li> elements required with string interpolation
    var liHtml = `<li class="mdl-list__item mdl-list__item--two-line" onclick="viewRegion(${index});">
                <span class="mdl-list__item-primary-content">
                  <span>${region.name}</span>
                  <span class="mdl-list__item-sub-title">Created at: ${region.createdAt}</span>
                </span>
              </li>`

    // Insert <li> element into <ul> node
    ulNode.insertAdjacentHTML('beforeend', liHtml)
  })
}

// Triggered when user clicks on a region in the list
function viewRegion(index){
  // Save the index selection into localstorage
  localStorage.setItem(APP_PREFIX + '.selectedRegion', index)
  // Redirect the user to viewRegion.html
  window.location.href = 'viewRegion.html'
}
